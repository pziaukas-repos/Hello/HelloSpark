#!/usr/bin/env python3
from pyspark.sql import SparkSession
from pyspark.sql.functions import month, dayofmonth


def main():
    # Getting data into a Spark session
    spark = SparkSession.builder.appName('basic').getOrCreate()
    df = spark.read.csv('data.csv', inferSchema=True, header=True)

    # Showing and collecting raw data
    df.printSchema()
    df.orderBy('height').show()
    df.select(['gender', 'height']).show()
    print(df.filter(df['age'] > 50).first().asDict())

    # Using pure SQL capabilities
    df.createOrReplaceTempView('basic_data')
    spark.sql('SELECT * FROM basic_data LIMIT 10').show()

    # Filtering
    df.filter(df['height'] < 170).show()
    df.filter((df['height'] > 180) & ~(df['gender'] == 'Male')).show()

    # Adjusting
    df.select([dayofmonth('date').alias('day'), 'first_name']).show()
    df.withColumn('month', month('date')).show()

    # Aggregating
    df.groupBy('gender').mean().show()
    df.agg({'height': 'mean'}).show()
    df.groupBy('gender').agg({'height': 'max'}).show()

    # Cleaning
    df.na.drop(subset=['height']).show()
    df.na.fill('Unknown', subset=['first_name', 'last_name']).show()


if __name__ == '__main__':
    main()
