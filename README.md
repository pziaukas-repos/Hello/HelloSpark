# Hello Spark

Spark demo project, inspired by http://spark.apache.org/docs/latest/sql-programming-guide.html .


## Prerequisites

Setup (Mac OS X)
```
brew install apache-spark
pipenv install
```


## Basic operations

```
./basic.py
```

Basic Spark usage example, including
* Showing and collecting raw data
* Using pure SQL capabilities
* Filtering
* Adjusting
* Aggregating
* Cleaning
